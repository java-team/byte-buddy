byte-buddy (1.14.19-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 29 Sep 2024 23:50:39 +0200

byte-buddy (1.14.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #1057493)
    - Refreshed the patches
    - Depend on libasm-java (>= 9.7)
  * Filter out the Linux libraries from the upstream tarball
  * Standards-Version updated to 4.7.0

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 09 Apr 2024 13:54:36 +0200

byte-buddy (1.12.23-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 11 Jun 2023 09:54:02 +0200

byte-buddy (1.12.21-1) unstable; urgency=medium

  * Team upload.
  * Remove the precompiled classes from the upstream tarball
  * Regenerate the precompiled classes
  * New upstream release
    - Refreshed the patches
    - Depend on libbyte-buddy-java (>= 1.11.22)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 12 Jan 2023 13:49:53 +0100

byte-buddy (1.11.22-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Depend on libbyte-buddy-java (>= 1.11.4)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 12 Jan 2023 10:30:34 +0100

byte-buddy (1.11.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 12 Jan 2023 00:07:18 +0100

byte-buddy (1.10.22-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Enabled CachedReturnPlugin when building byte-buddy-dep
    - Depend on libbyte-buddy-java (>= 1.8.22)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 11 Jan 2023 19:19:43 +0100

byte-buddy (1.9.16-1) unstable; urgency=medium

  * Team upload.
  * Filter out the Windows libraries from the upstream tarball
  * New upstream release
    - Refreshed the patches
    - Depend on libasm-java (>= 7.1)
    - New dependencies on build-helper-maven-plugin and jna-platform

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 11 Jan 2023 16:34:18 +0100

byte-buddy (1.8.22-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - Depend on libbyte-buddy-java (>= 1.8.15)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 11 Jan 2023 14:12:08 +0100

byte-buddy (1.8.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libbyte-buddy-java and relocate byte-buddy-maven-plugin
      at build time to avoid a circular dependency in the Maven reactor
    - Depend on libasm-java (>= 7.0)
    - New dependency on libmaven-bundle-plugin-java
  * Removed the unused dependency on libmaven-javadoc-plugin-java
  * Updated the watch file
  * Standards-Version updated to 4.6.2

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 10 Jan 2023 15:12:05 +0100

byte-buddy (1.8.2-3) unstable; urgency=medium

  * Team upload.
  * Drop javadoc package (Closes: #1025836)
  * Use debhelper-compat (=13)
  * Update Homepage URL
  * Set Rules-Requires-Root: no in debian/control
  * Bump Standards-Version to 4.6.1
  * Freshen years in debian/copyright

 -- tony mancill <tmancill@debian.org>  Sat, 10 Dec 2022 10:14:33 -0800

byte-buddy (1.8.2-2) unstable; urgency=medium

  * Team upload.
  * Build the byte-buddy-agent module

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 16 Sep 2019 23:02:58 +0200

byte-buddy (1.8.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libasm-java (>= 6.1)
    - Depend on liblombok-java (>= 1.16.20)
  * Standards-Version updated to 4.4.0
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 16 Sep 2019 18:43:37 +0200

byte-buddy (1.7.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Updated the files excluded from the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 12 Mar 2018 08:58:59 +0100

byte-buddy (1.7.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Updated the Maven rules
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 12 Feb 2018 23:16:31 +0100

byte-buddy (1.7.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libasm-java (>= 6.0-1~)
  * Added the missing Maven rules for eclipse-aether (Closes: #882052)
  * Standards-Version updated to 4.1.2
  * Simplified the Maven rules

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 12 Dec 2017 12:50:19 +0100

byte-buddy (1.7.2-1) unstable; urgency=medium

  * Team upload.

  [ Felix Natter ]
  * fix watch file (releases instead of tags, dversionmangle)
  * remove precompiled content from orig-source

  [ Emmanuel Bourg ]
  * New upstream release
  * Removed the unused build dependency on maven-assembly-plugin,
    maven-deploy-plugin, maven-install-plugin, maven-site-plugin
  * Removed the unused dependency on eclipse-aether (replaced by maven-resolver)
  * Ignore the maven-enforcer-plugin
  * Standards-Version updated to 4.1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 08 Sep 2017 12:19:52 +0200

byte-buddy (1.7.1-1) unstable; urgency=low

  * Initial release (Closes: #860325)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 23 Jul 2017 02:42:36 +0800
